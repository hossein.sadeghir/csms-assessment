# Running project

## Run on machine
First, install project dependencies by running this command.
```
npm install
```

After that, you can run project by running
```
npm start
```
Or start project in watch mode by
```
npm run start:dev
```

## Run in docker
Project contains docker file. To run the project in docker you need `docker` and `docker-compose` and you can run the project using this command:
```
docker-compose up
```

To shut down instance, you can use
```
docker-compose down
```

# Running the tests
To run project unit tests, use
```
npm test
```

And, to run e2e tests, run
```
npm run test:e2e
```

# Browsing APIs
Project exposes API documentation endpoint (Swagger UI) that you can see all the API specs, inputs, outputs and interact with APIs and test them.

You can use this UI in http://localhost:3000/api
