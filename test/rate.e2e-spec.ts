import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication, ValidationPipe } from '@nestjs/common';
import * as request from 'supertest';
import { RateModule } from './../src/rate/rate.module';

describe('Rate (e2e)', () => {
  let app: INestApplication;

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [RateModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    app.useGlobalPipes(new ValidationPipe({ transform: true }));
    await app.init();
  });

  it('/rate (POST)', () => {
    const inputJson = {
      rate: { energy: 0.3, time: 2, transaction: 1 },
      cdr: {
        meterStart: 1204307,
        timestampStart: '2021-04-05T10:04:00Z',
        meterStop: 1215230,
        timestampStop: '2021-04-05T11:27:00Z',
      },
    };

    return request(app.getHttpServer())
      .post('/rate')
      .send(inputJson)
      .expect(200)
      .expect({
        overall: 7.04,
        components: {
          energy: 3.276,
          time: 2.766,
          transaction: 1,
        },
      });
  });
});
