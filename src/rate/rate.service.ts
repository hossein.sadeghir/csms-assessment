import { Injectable } from '@nestjs/common';
import { CalculateRateOutput } from './dtos/calculate-rate.output';
import { ChargeDetailRecordInput } from './dtos/charge-detail-record.input';
import { RateInput } from './dtos/rate.input';
import { floor } from './utils';

@Injectable()
export class RateService {
  calculateRate(rate: RateInput, cdr: ChargeDetailRecordInput): CalculateRateOutput {
    // energy amount that driver consumed, input is in Wh and should be converted to kWh
    const consumedEnergy = (cdr.meterStop - cdr.meterStart) / 1000;

    // calculate duration in hours
    const timeMillis = cdr.timestampStop.getTime() - cdr.timestampStart.getTime();
    const timeHours = timeMillis / (60 * 60 * 1000);

    // calculate components floor to 3 decimal places
    const energyPrice = floor(consumedEnergy * rate.energy, 3);
    const timePrice = floor(timeHours * rate.time, 3);
    const transactionPrice = floor(rate.transaction, 3);

    // calculate total and floor to 2 decimal places
    const overall = floor(energyPrice + timePrice + transactionPrice, 2);

    return {
      overall,
      components: {
        energy: energyPrice,
        time: timePrice,
        transaction: transactionPrice,
      },
    };
  }
}
