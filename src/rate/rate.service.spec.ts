import { Test } from '@nestjs/testing';
import { ChargeDetailRecordInput } from './dtos/charge-detail-record.input';
import { RateInput } from './dtos/rate.input';
import { RateService } from './rate.service';

describe('RateService', () => {
  let rateService: RateService;

  beforeEach(async () => {
    const moduleRef = await Test.createTestingModule({
      providers: [RateService],
    }).compile();

    rateService = moduleRef.get(RateService);
  });

  describe('calculateRate', () => {
    it('should calculate rate for charging transaction', () => {
      // initialize inputs
      const rate: RateInput = { energy: 0.4, time: 2.5, transaction: 2 };
      const cdr: ChargeDetailRecordInput = {
        meterStart: 5106973,
        timestampStart: new Date('2021-12-02T10:48:09Z'),
        meterStop: 5118308,
        timestampStop: new Date('2021-12-02T13:09:57Z'),
      };

      // run method and test result
      expect(rateService.calculateRate(rate, cdr)).toEqual({
        overall: 12.44,
        components: {
          energy: 4.534,
          time: 5.908,
          transaction: 2,
        },
      });
    });
  });
});
