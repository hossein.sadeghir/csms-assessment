import { Body, Controller, HttpCode, Post } from '@nestjs/common';
import { ApiOkResponse, ApiTags } from '@nestjs/swagger';
import { CalculateRateInput } from './dtos/calculate-rate.input';
import { CalculateRateOutput } from './dtos/calculate-rate.output';
import { RateService } from './rate.service';

@ApiTags('Rate')
@Controller('/rate')
export class RateController {
  constructor(private readonly rateService: RateService) {}

  @Post()
  @HttpCode(200)
  @ApiOkResponse({ description: 'Rate calculation result', type: CalculateRateOutput })
  calculateRate(@Body() input: CalculateRateInput): CalculateRateOutput {
    return this.rateService.calculateRate(input.rate, input.cdr);
  }
}
