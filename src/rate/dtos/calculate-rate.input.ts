import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { ValidateNested } from 'class-validator';
import { ChargeDetailRecordInput } from './charge-detail-record.input';
import { RateInput } from './rate.input';

export class CalculateRateInput {
  @ApiProperty()
  @ValidateNested()
  @Type(() => RateInput)
  rate: RateInput;

  @ApiProperty()
  @ValidateNested()
  @Type(() => ChargeDetailRecordInput)
  cdr: ChargeDetailRecordInput;
}
