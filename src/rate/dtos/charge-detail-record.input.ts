import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { IsDate, IsNumber } from 'class-validator';

export class ChargeDetailRecordInput {
  @ApiProperty()
  @IsNumber()
  meterStart: number;

  @ApiProperty()
  @IsDate()
  @Type(() => Date)
  timestampStart: Date;

  @ApiProperty()
  @IsNumber()
  meterStop: number;

  @ApiProperty()
  @IsDate()
  @Type(() => Date)
  timestampStop: Date;
}
