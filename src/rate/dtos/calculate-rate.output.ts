import { ApiProperty } from '@nestjs/swagger';
import { RateComponentsOutput } from './rate-components.output';

export class CalculateRateOutput {
  @ApiProperty({ example: 7.04 })
  overall: number;

  @ApiProperty()
  components: RateComponentsOutput;
}
