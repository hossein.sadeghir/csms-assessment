import { ApiProperty } from '@nestjs/swagger';

export class RateComponentsOutput {
  @ApiProperty({ example: 3.276 })
  energy: number;

  @ApiProperty({ example: 2.766 })
  time: number;

  @ApiProperty({ example: 1 })
  transaction: number;
}
