import { ApiProperty } from '@nestjs/swagger';
import { IsNumber } from 'class-validator';

export class RateInput {
  @ApiProperty()
  @IsNumber()
  energy: number;

  @ApiProperty()
  @IsNumber()
  time: number;

  @ApiProperty()
  @IsNumber()
  transaction: number;
}
