# ===== Build stage
FROM node:16 as build

# Create app directory
WORKDIR /usr/src/app

# Copy files
COPY package*.json tsconfig.json tsconfig.build.json  ./
COPY src/ ./src

# Install dependencies and build
RUN npm ci
RUN npm run build
RUN npm prune --production

# ===== Run stage
FROM node:16

# Create app directory
WORKDIR /usr/src/app

# Copy dist and node_modules
COPY --from=build /usr/src/app/dist ./dist
COPY --from=build /usr/src/app/node_modules ./node_modules

# Expose port
EXPOSE 3000

# Run project
CMD [ "node", "dist/main.js" ]
